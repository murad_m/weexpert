<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Comment;
use App\Form\ArticleType;
use App\Form\CommentType;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ArticleController extends AbstractController
{
    public function createArticle(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $imageFile */
            $imageFile = $form['image']->getData();


            $originalFilename = pathinfo($imageFile->getClientOriginalName(), PATHINFO_FILENAME);
            // this is needed to safely include the file name as part of the URL
            $safeFilename = strtolower(iconv("UTF-8", "ISO-8859-1//TRANSLIT",
                $originalFilename));
            $newFilename = $safeFilename . '-' . uniqid() . '.' . $imageFile->guessExtension();

            // Move the file to the directory where brochures are stored
            try {
                $imageFile->move(
                    $this->getParameter('image_dir'),
                    $newFilename
                );
            } catch (FileException $e) {
                // ... handle exception if something happens during file upload
            }

            // updates the 'brochureFilename' property to store the PDF file name
            // instead of its contents
            $article->setImageFilename($newFilename);

            $article = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush();

            return $this->redirect('/view-article/' . $article->getId());

        }

        return $this->render(
            'article/edit.html.twig',
            array('form' => $form->createView())
        );

    }

    public function viewArticle($id)
    {
        $article = $this->getDoctrine()
            ->getRepository('App\Entity\Article')
            ->find($id);
        $article->addVisit();
        $em = $this->getDoctrine()->getManager();
        $em->persist($article);
        $em->flush();

        if (!$article) {
            throw $this->createNotFoundException(
                'The article you are searching for does not exist'
            );
        }
        return $this->render(
            'article/view.html.twig',
            array('article' => $article)
        );
    }

    public function showArticles(PaginatorInterface $paginator, Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $articles = $this->getDoctrine()
            ->getRepository('App\Entity\Article')
            ->findAll();

        /**
         * @var $paginator \Knp\Component\Pager\Paginator
         */
        $result = $paginator->paginate($articles, $request->query->getInt('page', 1), 10);
        return $this->render(
            'article/show.html.twig',
            array('articles' => $result)
        );
    }

    public function deleteArticle($id)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository('App\Entity\Article')->find($id);

        if (!$article) {
            throw $this->createNotFoundException(
                'There are no articles with the following id: ' . $id
            );
        }

        $em->remove($article);
        $em->flush();

        return $this->redirectToRoute('/landing_page');
    }

    public function updateArticle(Request $request, $id)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository('App\Entity\Article')->find($id);

        if (!$article) {
            throw $this->createNotFoundException(
                'There are no articles with the following id: ' . $id
            );
        }

        $form = $this->createForm(ArticleType::class, $article);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $imageFile = $form['image']->getData();


            $originalFilename = pathinfo($imageFile->getClientOriginalName(), PATHINFO_FILENAME);
            // this is needed to safely include the file name as part of the URL
            $safeFilename = strtolower(iconv("UTF-8", "ISO-8859-1//TRANSLIT",
                $originalFilename));
            $newFilename = $safeFilename . '-' . uniqid() . '.' . $imageFile->guessExtension();

            // Move the file to the directory where brochures are stored
            try {
                $imageFile->move(
                    $this->getParameter('image_dir'),
                    $newFilename
                );
            } catch (FileException $e) {
                // ... handle exception if something happens during file upload
            }

            // updates the 'brochureFilename' property to store the PDF file name
            // instead of its contents
            $article->setImageFilename($newFilename);
            $article = $form->getData();
            $em->flush();
            return $this->redirect('/view-article/' . $id);
        }

        return $this->render(
            'article/edit.html.twig',
            array('form' => $form->createView())
        );
    }

    public function listComments(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository('App\Entity\Article')->find($id);
        if (!$article) {
            throw $this->createNotFoundException(
                'There are no articles with the following id: ' . $id
            );
        }
        $comments = $article->getComments();
        return $this->render(
            'article/comments.html.twig',
            array('comments' => $comments, 'article' => $article)
        );
    }

    public function addComment(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository('App\Entity\Article')->find($id);
        if (!$article) {
            throw $this->createNotFoundException(
                'There are no articles with the following id: ' . $id
            );
        }
        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $comment = $form->getData();
            $comment->setCreatedAt();
            $article->addComment($comment);

            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->flush();
            return $this->redirectToRoute('list-comments', ['id' => $article->getId()]);
        }
        return $this->render(
            'article/add_comment.html.twig',
            array('form' => $form->createView())
        );
    }

    public function deleteComment(Request $request, $comment, $article)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository('App\Entity\Article')->find($article);
        $child_comment = $em->getRepository('App\Entity\Comment')->find($comment);
        $article->removeComment($child_comment);
        $em = $this->getDoctrine()->getManager();
        $em->persist($article);
        $em->flush();
        return $this->redirectToRoute('list-comments', ['id' => $article->getId()]);
    }
}