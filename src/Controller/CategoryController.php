<?php

namespace App\Controller;

use App\Entity\Category;
use App\Form\CategoryType;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends AbstractController
{
    public function createCategory(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $category = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();

            return $this->redirect('/view-category/' . $category->getId());

        }

        return $this->render(
            'category/edit.html.twig',
            array('form' => $form->createView())
        );

    }

    public function viewCategory($id, PaginatorInterface $paginator, Request $request)
    {
        $category = $this->getDoctrine()
            ->getRepository('App\Entity\Category')
            ->find($id);
        $category->addVisit();
        $em = $this->getDoctrine()->getManager();
        $em->persist($category);
        $em->flush();

        if (!$category) {
            throw $this->createNotFoundException(
                'The $category you are searching for does not exist'
            );
        }

        $articles = $category->getArticles();
        /**
         * @var $paginator \Knp\Component\Pager\Paginator
         */
        $result = $paginator->paginate($articles, $request->query->getInt('page', 1), 10);

        return $this->render(
            'category/view.html.twig',
            array(
                'category' => $category,
                'articles' => $result
            )
        );
    }

    public function showCategories(PaginatorInterface $paginator, Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $categories = $this->getDoctrine()
            ->getRepository('App\Entity\Category')
            ->findAll();

        /**
         * @var $paginator \Knp\Component\Pager\Paginator
         */
        $result = $paginator->paginate($categories, $request->query->getInt('page', 1), 10);
        return $this->render(
            'category/show.html.twig',
            array('categories' => $result)
        );
    }

    public function deleteCategory($id)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository('App\Entity\Category')->find($id);

        if (!$category) {
            throw $this->createNotFoundException(
                'The category you are requesting does not exist'
            );
        }

        $em->remove($category);
        $em->flush();

        return $this->redirectToRoute('/landing_page');
    }

    public function updateCategory(Request $request, $id)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository('App\Entity\Category')->find($id);

        if (!$category) {
            throw $this->createNotFoundException(
                'The category you are requesting does not exist'
            );
        }

        $form = $this->createForm(CategoryType::class, $category);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            return $this->redirect('/view-category/' . $id);
        }

        return $this->render(
            'category/edit.html.twig',
            array('form' => $form->createView())
        );
    }
}
