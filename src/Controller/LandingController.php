<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity;

class LandingController extends AbstractController
{
    /**
     * @Route("/landing", name="landing")
     */
    public function indexAction()
    {
        $categories = $this->getDoctrine()
            ->getRepository('App\Entity\Category')
            ->findAll();
        $landingCats = [];
        $landingArts = [];
        $i = 0;
        foreach ($categories as $cat) {
            $landingCats[$i] = $cat;
            $landingArts[$i] = $cat->getLandingArticles();
            $i++;
        }
        return $this->render('landing/index.html.twig',
            array('categories' => $landingCats, 'articles' => $landingArts));
    }
}
