<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Dotenv\Dotenv;
use Swift_Mailer;
use App\Repository;


class SendTopCommand extends Command
{
    protected static $defaultName = 'send-top';

    protected function configure()
    {
        $this
            ->setDescription('Sends top 10 stats to email   ')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument email to send stats to');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $arg1 = $input->getArgument('arg1');
        $dotenv = new Dotenv();
        $dotenv->load(__DIR__ . '/.env');
        $mailer = new Swift_Mailer();
        if (!$arg1) {
            $arg = 'defaultmail@mail.ee';
        }
        $cr = new Repository\CategoryRepository();
        $ar = new Repository\ArticleRepository();

        $message = (new \Swift_Message('Hello Email'))
            ->setFrom($_ENV['DB_USER'])
            ->setTo($arg1)
            ->setBody(
                $this->renderView(
                    'emails/top10html.twig',
                    [
                        'articles' => $ar->findTop10(),
                        'categories' => $cr->findTop10()
                    ]
                ),
                'text/html'
            )

            // you can remove the following code if you don't define a text version for your emails
            ->addPart(
                $this->renderView(
                // templates/emails/registration.txt.twig
                    'emails/registration.txt.twig',
                    ['name' => $name]
                ),
                'text/plain'
            );

        $mailer->send($message);

        $io->success('You have a new command! Now make it your own! Pass --help to see your options.');

        return 0;
    }
}
