<?php

namespace App\Entity;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Article;

/**
 * @ORM\Table(name="categories")
 * @ORM\Entity(repositoryClass="App\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * Many Categories have Many Articles.
     * @ORM\ManyToMany(targetEntity="Article", mappedBy="categories")
     * @ORM\OrderBy({"createdAt" = "ASC"})
     */
    private $articles;

    /**
     * @var int
     *
     * @ORM\Column(name="visit", type="integer", nullable=false, options={"unsigned":true, "default":0}))
     */
    private $visit;

    public function __construct()
    {
        $this->news = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function addArticles(Article $article)
    {
        $article->addCategory(this);
        $this->news[] = $article;
    }

    public function getArticles()
    {
        return $this->articles;
    }

    public function getLandingArticles()
    {
        $criteria = Criteria::create()
            ->setMaxResults(3);
        return $this->articles->matching($criteria);
    }

    public function addVisit()
    {
        $this->visit = $this->visit++;
    }

    public function getVisit(): ?int
    {
        return $this->visit;
    }

}
